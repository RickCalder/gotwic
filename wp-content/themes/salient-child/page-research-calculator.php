<?php
/**
 * The template for displaying pages.
 *
 * @package Salient WordPress Theme
 * @version 9.0.2
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header();
nectar_page_header( $post->ID );

$nectar_fp_options = nectar_get_full_page_options();

global $wpdb;
$research = $wpdb->get_results("SELECT * from research");

?>

<div class="container-wrap">
	<div class="<?php if ( $nectar_fp_options['page_full_screen_rows'] != 'on' ) { echo 'container';} ?> main-content">
		<div class="row">
			
			<?php

			// Yoast breadcrumbs.
			if ( function_exists( 'yoast_breadcrumb' ) && ! is_home() && ! is_front_page() ) {
				yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' ); }

			 // Buddypress related.
			global $bp;
			if ( $bp && ! bp_is_blog_page() ) {
				echo '<h1>' . get_the_title() . '</h1>';
			}

			 // Fullscreen row option.
			if ( $nectar_fp_options['page_full_screen_rows'] == 'on' ) {
				echo '<div id="nectar_fullscreen_rows" data-animation="' . esc_attr( $nectar_fp_options['page_full_screen_rows_animation'] ) . '" data-row-bg-animation="' . esc_attr( $nectar_fp_options['page_full_screen_rows_bg_img_animation'] ) . '" data-animation-speed="' . esc_attr( $nectar_fp_options['page_full_screen_rows_animation_speed'] ) . '" data-content-overflow="' . esc_attr( $nectar_fp_options['page_full_screen_rows_content_overflow'] ) . '" data-mobile-disable="' . esc_attr( $nectar_fp_options['page_full_screen_rows_mobile_disable'] ) . '" data-dot-navigation="' . esc_attr( $nectar_fp_options['page_full_screen_rows_dot_navigation'] ) . '" data-footer="' . esc_attr( $nectar_fp_options['page_full_screen_rows_footer'] ) . '" data-anchors="' . esc_attr( $nectar_fp_options['page_full_screen_rows_anchors'] ) . '">';
			}

			if ( have_posts() ) :
				while ( have_posts() ) :

					the_post();

					the_content();

				 endwhile;
			 endif;

			if ( $nectar_fp_options['page_full_screen_rows'] == 'on' ) {
				echo '</div>';
			}
      ?>
      <div class="row">
        <div class="row">
          <label for="bonus">Research Bonus</label>
          <input type="text" id="bonus" placeholder="Enter your Research Bonus">
        </div>
        <div class="row">
          <label for="castle">Castle Level</label>
          <select id="castle" name="castle">
            <?php 
              for( $x=1; $x < 26; $x++) {
                echo '<option value="' . $x . '">Level ' . $x . '</option>';
              }
            ?>
            
          </select>
        </div>
        <div class="row">
          <label for="vip">VIP Level</label>
          <select id="vip" name="vip">
            <option value="300">Level 1</option>
            <option value="420">Level 2</option>
            <option value="540">Level 3</option>
            <option value="660">Level 4</option>
            <option value="780">Level 5</option>
            <option value="900">Level 6</option>
            <option value="1140">Level 7</option>
            <option value="1260">Level 8</option>
            <option value="1440">Level 9</option>
            <option value="1620">Level 10</option>
            <option value="1800">Level 11</option>
            <option value="2100">Level 12</option>
            <option value="2400">Level 13</option>
            <option value="2700">Level 14</option>
            <option value="3600">Level 15</option>
          </select>
        </div>
        <div class="row">
          <label for="tree">Research Tree</label>
          <select id="tree">
            <option value="select">Select Tree</option>
            <option value="Production">Production</option>
            <option value="City Defense">City Defense</option>
            <option value="Military">Military</option>
            <option value="Expedition & Pacification">Expedition &amp; Pacification</option>
            <option value="Advanced Defense">Advanced Defense</option>
            <option value="Advanced Military">Advanced Military</option>
            <option value="Formations">Formations</option>
            <option value="Commandership">Commandership</option>
          </select>
        </div>
        <div class="row">
          <label for="research">Research</label>
          <select id="research"></select>
        </div>
      </div>
      <div class="row">
        <h3>Base time: <span id="seconds"></span></h3> 
        <h3>Your time: <span id="seconds2"></span></h3> 
      </div>
      <div class="row" id="helpstable">
        <table>
          <thead>
            <tr>
              <th>Help</th>
              <th>Time Remaining</th>
            </tr>
          </thead>
          <tbody id="helps"></tbody>
        </table>
      </div>
		</div><!--/row-->
	</div><!--/container-->
</div><!--/container-wrap-->

<?php get_footer(); ?>

<script>
  const research = <?php echo json_encode($research); ?>;
  let tree
  jQuery("#tree").change(function() {
    tree = jQuery("#tree").val()
    let $element = jQuery('#research')
    $element.empty()
    $element.append('<option value="">Select Research</option>')
    research.forEach(function(el) {
      if( tree === el.tree ){
        $element.append('<option value="' + el.research + " Level " + el.level + '" data-seconds="' + el.seconds + '"> ' + el.research + " Level " + el.level + '</option>')
      }
    })
    calculateTimes()
  })

  jQuery("#research").change(function(){
    calculateTimes()
  })
  jQuery("#castle").change(function(){
    calculateTimes()
  })
  jQuery("#bonus").change(function(){
    calculateTimes()
  })
  jQuery("#vip").change(function(){
    calculateTimes()
  })

  function calculateTimes() {
    jQuery('#seconds').val('')
    jQuery('#seconds2').val('')
    jQuery("#seconds").text( '' )
    jQuery("#seconds2").text( '' )
    if( typeof jQuery("#research").find(':selected').data('seconds') === 'undefined' ) return
    const vipTime = jQuery('#vip').find(':selected').val()
    let seconds = parseInt(jQuery("#research").find(':selected').data('seconds'))
    let numdays = Math.floor(seconds / 86400);
    let numhours = Math.floor((seconds % 86400) / 3600);
    let numminutes = Math.floor(((seconds % 86400) % 3600) / 60);
    let numseconds = ((seconds % 86400) % 3600) % 60;
    let total = numdays + 'd : ' + numhours + 'h : ' + numminutes  + 'm : ' + numseconds + 's'

    let spedSeconds = Math.round(seconds * 100 / (100 + parseFloat(jQuery('#bonus').val())))
    let spedDays = Math.floor(spedSeconds / 86400);
    let spedHours = Math.floor((spedSeconds % 86400) / 3600);
    let spedMins = Math.floor(((spedSeconds % 86400) % 3600) / 60);
    let spedSecs = ((spedSeconds % 86400) % 3600) % 60;
    let spedTotal = spedDays + 'd : ' + spedHours + 'h : ' + spedMins  + 'm : ' + Math.floor(spedSecs) + 's'
    if( spedSeconds - vipTime <= 0 ) {
      spedTotal = 'Free'
    }

    jQuery("#seconds").text( total )
    jQuery("#seconds2").text( spedTotal )
    if( spedTotal === 'Free' ) return
    const helps = 5 + parseInt(jQuery('#castle').find(':selected').val())

    let newSeconds = spedSeconds
    let html = ''
    for( i=0; i <= helps -1; i++ ) {
      if( newSeconds * 0.01 < 60 ) {
        newSeconds = newSeconds - 60    
        let newDays = Math.floor(newSeconds / 86400);
        let newHours = Math.floor((newSeconds % 86400) / 3600);
        let newMins = Math.floor(((newSeconds % 86400) % 3600) / 60);
        let newSecs = ((newSeconds % 86400) % 3600) % 60;
        let newTotal = newDays + 'd : ' + newHours + 'h : ' + newMins  + 'm : ' + Math.floor(newSecs) + 's'
        if(newSeconds - vipTime <= 0) {
          newTotal = 'Free'
        }
        html += '<tr><td>Help ' + parseInt(i + 1) + '</td><td> ' + newTotal + '</td></tr>'
      } else {
        newSeconds = newSeconds - (newSeconds * 0.01)    
        let newDays = Math.floor(newSeconds / 86400);
        let newHours = Math.floor((newSeconds % 86400) / 3600);
        let newMins = Math.floor(((newSeconds % 86400) % 3600) / 60);
        let newSecs = ((newSeconds % 86400) % 3600) % 60;
        let newTotal = newDays + 'd : ' + newHours + 'h : ' + newMins  + 'm : ' + Math.floor(newSecs) + 's'
        html += '<tr><td>Help ' + parseInt(i + 1) + '</td><td> ' + newTotal + '</td></tr>'
      }
    }
      jQuery('#helps').empty().append(html)


  }
  
  
</script>